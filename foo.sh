#!/bin/bash

# A simple test task

# do something ...  may access the ALIPERF framework environment by

echo "executing a test task for alidist"

# make a simple metrics
echo "tests,host=alibicompute01.cern.ch,alidist=70f5ea9,o2=2f7f7e9 value=406" > metric 

submit_to_AliPerf_InfluxDB $metric
