#!/bin/bash

###########################################
# Task performing a TPC CUDA reco
###########################################

today=`date +%d-%m-%Y-%H:%M`
echo "Doing TPC CUDA reco bench for ALIDIST:${ALIPERF_ALIDISTCOMMIT}_O2:${ALIPERF_O2COMMIT}"

# launch a simulation
o2-sim --skipModules ZDC,MFT -n 20 -g pythia8

# launch the digitization
o2-sim-digitizer-workflow -b -run --tpc-lanes 12 --onlyDet TPC

# launch the reconstruction
o2-tpc-reco-workflow -b -run --tpc-digit-reader "--infile tpcdigits.root" --input-type digits --output-type clusters,tracks  --tpc-track-writer "--treename events --track-branch-name Tracks --trackmc-branch-name TracksMCTruth" --tracker-options "gpuType=CUDA" &> logtpc

# upload rest to EOS (use framework function)
init_EOS

# could push stuff to EOS here


# upload result to InfluxDB
METRIC="tpcrecoCUDAtime,conf=pythia8_20pp,host=alibicompute01.cern.ch,alidist=${ALIPERF_ALIDISTCOMMIT},o2=${ALIPERF_O2COMMIT} value=${value}"
echo "${METRIC}" > metrics.dat

# send it to Influx (use framework function)
# submit_to_AliPerf_InfluxDB metrics.dat
