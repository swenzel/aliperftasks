#!/bin/sh
set -x
ls
pwd

# enable dev toolset
. /opt/rh/devtoolset-7/enable

export EOS_MGM_URL=root://eosuser.cern.ch
# get a valid kerberos token for aliperf and write to EOS
kinit  -k  -t /home/swenzel/cron_jobs/aliperf.keytab2020 aliperf@CERN.CH
klist

export HOME=/home/swenzel/

# setup the sources in a dedicated workspace
cd /home/swenzel/
cd $(mktemp -d -p .)

mkdir sw

# copy the git sources from somewhere else as this needs time
cp -r /home/swenzel/alisw/sw/MIRROR sw
# cp -r /home/swenzel/alisw/sw/TARS sw

# checkout the latest source code and the latest recipes
[[ -d "alidist" ]] && rm -r -f alidist
git clone https://github.com/alisw/alidist
[[ -d "alibuild" ]] && rm -r -f alibuild
git clone --branch v1.6.0.rc2 https://github.com/alisw/alibuild
[[ -d "O2" ]] && rm -r -f O2
git clone https://github.com/AliceO2Group/AliceO2 O2

cp /home/swenzel/alisw/alidist/pp_performance_test.sh alidist

# determine commits
export ALIDISTCOMMIT=`cd alidist && git rev-parse --short HEAD`
export O2COMMIT=`cd O2 && git rev-parse --short HEAD`

# compile and run the benchmark
./alibuild/aliBuild build pp_performance_test --defaults o2 --debug --no-auto-cleanup

today=`date +%d-%m-%Y-%H:%m`

DIR=sw/BUILD/pp_performance_test-latest/pp_performance_test/
set -x
if [[ -d "${DIR}" ]]
then
  # tar up the build artefacts and send them to EOS
  cd ${DIR}

  TARFILE=artefacts_pp_perf_${today}_ALIDIST:${ALIDISTCOMMIT}_O2:${O2COMMIT}.tar.gz

  # tar up everything and sent to EOS
  tar --force-local -czf ${TARFILE} *

  eos cp ${TARFILE} /eos/user/a/aliperf/simulation/O2SimBenches/

  eos mkdir /eos/user/a/aliperf/alibi/${TARGETDIR}
  eos cp * /eos/user/a/aliperf/alibi/${TARGETDIR}
fi


# in the pp setting we also check the geometry
${HOME}/cron_jobs/checkgeometry.sh
